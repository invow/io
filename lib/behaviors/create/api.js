/**
 * @Dependencies
 */
const invow = require('../../index.js');
const spawn = require('child_process').spawn;
const inquirer = require('inquirer');

/**
 * @Run module
 */
const RUN = () => {
  /**
   * @Questions
   */
  const questions = 
  [{
    type: 'list',
    name: 'choice',
    message: "Which technology?",
    choices: [
      "Node.js-Express",
      "Python-Django"
    ]
  }];

  /**
   * @Answers
   * @Command response
   */
  inquirer.prompt(questions).then(answers => {
    let command = `yo`;
    switch(answers['choice']) {
      //Node.js-Express case
      case questions[0].choices[0]:
      spawn(
        command,
        ['rest'],
        { stdio:'inherit' }
      )
        .on('close', function () {
          //closed
        });
      break;
      //Python-Django case
      case questions[0].choices[1]:
      spawn(
        command,
        ['djangospa'],
        { stdio:'inherit' }
      )
        .on('close', function () {
          //closed
        });
      break;
    }
  })
}

exports.RUN = RUN;