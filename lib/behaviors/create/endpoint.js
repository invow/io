/**
 * @Dependencies
 */
const invow = require('../../index.js');
const spawn = require('child_process').spawn;

/**
 * @Run module
 */
const RUN = () => {
  /**
   * @Command response
   */
  let command = `yo`;
  spawn(
    command,
    [`rest:api`],
    { stdio:'inherit' }
  )
  .on('close', function () {
    invow.say('Finished.')
  });
}

exports.RUN = RUN;