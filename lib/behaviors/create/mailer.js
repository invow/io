/**
 * @Dependencies
 */
const invow = require('../../index.js');
const clone = require('git-clone');

/**
 * @Run module
 */
const RUN = () => {
  /**
   * @Command response
   */
  invow.say('Installing Mailer developed using Node.js ...');
  clone('https://invow@bitbucket.org/invow/mailer.git', 'mailer',
    (err) => {
      if(!err)
        invow.say('Done.');
        invow.say('Run: $ cd mailer/ && npm install && npm start');
    }
  );
}

exports.RUN = RUN;