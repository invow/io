/**
 * @Dependencies
 */
const invow = require('../../index.js');
const inquirer = require('inquirer');
const spawn = require('child_process').spawn;

/**
 * @Run module
 */
const RUN = () => {

  /**
   * @Questions
   */
  const questions = [{
    type: 'list',
    name: 'environment',
    message: "Which environment?",
    choices: [
      "dev",
      "prod"
    ]
  }];

  /**
   * SpawnCommand
   * @param {Array} answer 
   * @param {String} command 
   */
  const spawnCommand = (answer, command) => {
    spawn(
      command,
      answer,
      { stdio:'inherit' }
    )
    .on('close', function () {
      invow.say('Command executed.');
    });
  };

  /**
   * @Answers
   * @Command response
   */
  inquirer.prompt(questions)
    .then(
      answers => {
        const answer = answers['environment'];
        switch(answer) {
          case questions[0].choices[0]:
            const commandDevNpm = 'npm'
            const answerDevNpm = ['run', 'build']
            spawnCommand(answerDevNpm, commandDevNpm);
          break;
          case questions[0].choices[1]:
            const commandProdNpm = 'npm'
            const answerProdNpm = ['run', 'build']
            spawnCommand(answerProdNpm, commandProdNpm);
          break;
        }
        invow.say('Finished.');
      });
}

exports.RUN = RUN;