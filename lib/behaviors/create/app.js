/**
 * @Dependencies
 */
const invow = require('../../index.js');
const inquirer = require('inquirer');
const spawn = require('child_process').spawn;
const clone = require('git-clone');

/**
 * @Run module
 */
const RUN = () => {
  /**
   * @Question
   */
  const question = 
  [{
    type: 'list',
    name: 'app',
    message: "What kind of app?",
    choices: [
      "mobile",
      "web"
    ]
  }];
  /**
   * @QuestionWeb
   */
  const questionWeb = [{
      type: 'list',
      name: 'tech',
      message: "Wich technology?",
      choices: [
        "Brandom (React web consuming RandomAPI)",
        "Laikapp (Angular Ngx consuming SwAPI)"
      ]
    }];



  /**
   * @Answers
   * @Command
   */
  inquirer.prompt(question).then(answers => {
    switch(answers['app']) {
      //Mobile case
      case question[0].choices[0]:
      let expoCommand = `expo`;
      spawn(
        expoCommand,
        ['init'],
        { stdio:'inherit' }
      )
      .on('close', function () {
        invow.say('Finished');
      });
      break;
      //Web case
      case question[0].choices[1]:
      inquirer.prompt(questionWeb).then(answers => {
        switch(answers['tech']) {
          //Brandom
          case questionWeb[0].choices[0]:
          invow.say('Installing Brandom React Web app...');
          clone('https://invow@bitbucket.org/invow/brandom.git', 'brandom-web',
            (err) => {
              if(!err)
                invow.say('Done.');
                invow.say('Run: $ cd brandom-web/ && npm install && npm start');
            }
          );
          break;
          //Laikapp
          case questionWeb[0].choices[1]:
          invow.say('Installing Laikapp Angular Ngx Web app...');
          clone('https://invow@bitbucket.org/invow/laikapp.git', 'laikapp-web',
            (err) => {
              if(!err)
                invow.say('Done.');
                invow.say('Run: $ cd laikapp-web/ && npm install && npm start');
            }
          );
          break;
        }
      });
      break;
    }
  });
}

exports.RUN = RUN;