// ./lib/index.js
const colors  = require('./colors');

/**
 * Displays a string in the console
 * 
 * @param {string_to_say} String string to show in the console
 */
const say = (string_to_say) => {
  return console.log(colors.get.FgMagenta, string_to_say);
};

const run = (action, behavior) => {
  const behaviors = `${__dirname}/behaviors/${action}/`;
  const fs = require('fs');
  fs.readdir(behaviors, (err, files) => {
    files.forEach(file => {
      dependency = file.replace('.js', '');
      if (dependency === behavior) {
        let behavior = require(`./behaviors/${action}/${dependency}`);
        behavior.RUN(action);
      }
    });
  });
};

exports.say = say;
exports.run = run;