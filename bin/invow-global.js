#!/usr/bin/env node
const invow = require('../lib/index.js');
const config = require('../invow.json');

// Delete the 0 and 1 argument (node and script.js)
const args = process.argv.splice(process.execArgv.length + 2);

// Retrieve arguments
const action = args[0];
const behavior = args[1];
let actions = config.actions;

getBehaviors = (action, behaviors) => {
    for (let index = 0; index < behaviors.length; index++) {
        switch(behavior) {
            case behaviors[index]:
            invow.run(action, behaviors[index]);
            break;
        }      
    }
};

(getActions = () => {
    actions.forEach(
        (item) => {
            switch(action) {
                case item.action:
                getBehaviors(item.action, item.behaviors);
                break;
            }
        }
    );
})();

