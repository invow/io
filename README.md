# Invow I/O

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Invow I/O is a software factory tool from developers to developers. 
By using Invow you can:

  - Create projects
  - Create apps
  - Create Invow's commands

### Installation

Invow requires [Node.js](https://nodejs.org/) v10+ to run.

Install Invow globally:

```sh
$ npm i -g invow
```
### Plugins

Invow is currently extended with the following plugins:

| Command | Dependency |
| ------ | ------ |
| invow create app | MOBILE: `invow install expo` |
| invow create app | WEB: `Brandom` (React Web app) needs `git` on your system and maybe `CORS` browser extension enabled to consume API in `dev` |
| invow create app | WEB: `Laikapp` (Angular Ngx Web app) needs `git` on your system and maybe `CORS` browser extension enabled to consume the API in `dev` |
| invow create api | Django: `invow install yeoman && invow install gulp && invow install djangospa` |
| invow create api | Node.js: `invow install yeoman && invow install node-rest` |
| invow create mailer | Needs `git` on your system |

### Available commands

Invow is currently extended with the following plugins:

| Command | Description |
| ------ | ------ |
| invow install expo | Install expo client `globally` to create mobile apps|
| invow install yeoman | Install yeoman `globally` for generate different kind of apps |
| invow install node-rest | A `yeoman` rest generator developed using Node.js `globally` |
| invow install nativeBase | A base UI components for React Native apps |
| invow install gulp | Install Gulp `globally` |
| invow install djangospa | Install `yeoman` djangospa generator `globally` |

---

| Command | Description |
| ------ | ------ |
| invow create app | Generate a client app (mobile or web) |
| invow create api | Generate a server app using [django] or [nodejs] |
| invow create endpoint | Placed on your `node-rest` api, it creates a new endpoint |
| invow create mailer | Generate a `nodemailer` app |
| invow create behavior | Create a new command. Ex: `invow [action] your_behavior` |


### Creating a behavior

You can create your own behavior. First of all you need to `add your behavior's name` into  the `invow.json` file (choose in wich action you want to add your new behavior). Then in your terminal:
```sh
$ invow create behavior
```
When you create a new behavior, a new `.js file` is created in `lib/behaviors/[action]/your_behavior.js` with a `module's scaffolding struture`.
Now you can add all the logic you need there.

### Trying your new a behavior

You can now try your new behavior in your terminal:
```sh
$ invow [action] your_behavior
```
### Push your new behavior to the community

You can push your new behavior into the community just by creating your branch from master and then pushing it to the repository.



